
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class DogInfoProgramTest {

	@Test

	public void testgetName() {
		DogInfoProgram doginfoprogram = new DogInfoProgram();
		String result = doginfoprogram.getName("dogname", "dogbreed");
	    assertEquals("dognamedogbreed", result);

	}

	@Test

	public void testGet2() {
		DogInfoProgram doginfoprogram = new DogInfoProgram();
		int dogage = 49;
		assertEquals(dogage, doginfoprogram.get2(5, 7));

	}

	@Test

	public void testGet2returnFalse() {
		DogInfoProgram doginfoprogram = new DogInfoProgram();
		int dogyear = 47;
		assertNotEquals(dogyear, doginfoprogram.get2(5, 7));

	}

	@Test

	public void testDogbreed() {
		DogInfoProgram doginfoprogram1 = new DogInfoProgram();
		DogInfoProgram doginfoprogram2 = new DogInfoProgram();
		String[] breed1 = doginfoprogram1.dogbreed();
		String[] breed2 = doginfoprogram2.dogbreed();
		assertNotEquals(breed1, breed2);

	}

}
